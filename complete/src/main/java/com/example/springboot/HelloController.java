package com.example.springboot;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import static net.logstash.logback.argument.StructuredArguments.keyValue;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index(@RequestHeader("accept") String acceptHeader) {
		LoggerFactory.getLogger(getClass()).info("Hello", keyValue("accept", acceptHeader));
		return "Greetings from Spring Boot!";
	}

}
